ct_filt <- 
  filter_clause_ga4(
    list(
      dim_filter(
        "pagePath", "REGEX", "^/search$"
      ),
      dim_filter(
        "eventCategory", "REGEX", "Card Click"
      )
    ),
    operator = "AND"
  )
cts <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("totalEvents", "uniqueEvents"), 
                   dimensions = c("eventCategory", "eventAction", "eventLabel", "pagePath"),
                   dim_filters = ct_filt,
                   anti_sample = TRUE) 

cts %>% 
  group_by(
    eventCategory,
    eventAction
  ) %>% 
  summarise(t = n()) %>% 
  ungroup() %>% 
  mutate(per = t / sum(t))
