library(tidyverse)
library(modeafun)
library(modeasecure)
library(scales)
library(ggrepel)
library(glue)
library(lubridate)
library(kableExtra)
library(wordcloud)
library(googleAnalyticsR)
# create our classifier function for Carilion Clinic URLs.
classifier_cc <- 
  function(x){
    case_when(
      x == "(not set)" ~ "exit",
      x == "(entrance)" ~ "entrance",
      str_count(x, "/") > 1 ~ substr(x, 2, str_locate(x, "/[a-z]+/")[,"end"]-1),
      x == "/" ~ "homepage",
      TRUE ~ substr(x, 2, str_length(x))
    )
  }

week_format <- 
  function(df, x){
    df %>%
      mutate(
        week = isoweek(date),
        year = year(date),
        month = month(date),
        week_index =
          case_when(# week counts roll over past 52 if >1 year
            month == 1 & week > 50 ~ week,
            year != min(year) ~ week + (max(week) * (year - min(year))),
            TRUE ~ week  # otherwise, make sure weeks at end of year are counted correctly.
          )
      )
  }
date_range <- c(Sys.Date() %m-% months(3), Sys.Date())

GA_pull_discovery <- 
  modeasecure::GA_pull_factory(
    email = "analytics.team@modea.com",
    view_id = 15128437,
    dates = date_range
  )
# create this filter for the conversion events if needed.
conv_filt <- filter_clause_ga4(list(dim_filter("eventCategory", "EXACT", "Global Conversions")))
fill_stye <- scale_fill_stata()
color_style <- scale_color_stata()

######## data pulls #########
discovery_sessions <- GA_pull_discovery(
  metrics = c(
    "sessions",
    "bounces",
    "avgSessionDuration",
    "pageviews"
  ),
  dimensions = c(
    "medium",
    "campaign",
    "source",
    "country",
    "region",
    "deviceCategory",
    "browser"
  )
)

discovery_pages <- GA_pull_discovery(
  metrics = c("pageviews"),
  dimensions = c("userType", "pagePath")
)

discovery_temporal <- GA_pull_discovery(
  metrics = c("sessions"),
  dimensions = c("userType", "date")
)

discovery_loadtime <- GA_pull_discovery(
  metrics = c(
    "avgPageLoadTime",
    "bounces",
    "sessions",
    "pageviews"
  ),
  dimensions = c("deviceCategory", "browser")
)

discovery_demographics <- GA_pull_discovery(
  metrics = c("sessions"),
  dimensions = c("userAgeBracket", "userGender")
)

discovery_landing <- GA_pull_discovery(
  dimensions = c("landingPagePath", "secondPagePath"),
  metrics = c("sessions", "bounces")
)

hits <- 
  GA_pull_discovery(
    dimensions = c("pagePath","date","source","medium","campaign"),
    metrics = c("pageviews", "uniquePageviews"),
  ) %>% 
  mutate(
    type = classifier_cc(page_path)
  ) %>% 
  group_by(type, date, source, medium, campaign) %>% 
  summarise(
    pv = sum(pageviews)
  ) %>% 
  group_by(type) %>% 
  mutate(group_total = sum(pv))
sessions <- 
  GA_pull_discovery(
    dimensions = c("date","source","medium","campaign"),
    metrics = c("sessions"),
  )

conversion <- 
  GA_pull_discovery(
    dimensions = c(
      "eventAction",
      "pagePath",
      "date",
      "source",
      "medium",
      "campaign"
    ),
    metrics = c(
      "totalEvents",
      "uniqueEvents"
    ),
    dim_filters = conv_filt
  ) %>% 
  mutate(
    type = classifier_cc(page_path) ### this is our classifer function to break out the pages into page types for easier summarization.
  ) %>% 
  group_by(
    type,
    event_action,
    date,
    source,
    medium,
    campaign
  ) %>% 
  summarise(
    conversions = sum(total_events)
  ) %>% 
  pivot_wider(
    names_from = event_action,
    values_from = conversions,
    values_fill = 0
  )

campaign_perf <- 
  merge(
  sessions,
  conversion,
  by = c("date", "campaign", "source", "medium")
) %>% 
  clean_names() %>% 
  mutate(total_conv = make_appointment+click_to_call) %>% 
  group_by(campaign, source, medium, date) %>% 
  summarise(
    total_conv = sum(total_conv, na.rm = TRUE),
    sessions = sum(sessions, na.rm = TRUE)
  )
n_distinct(campaign_perf$campaign)

campaign_perf %>% 
  filter(sessions >= 50) %>%
  mutate(
    rate= total_conv/sessions
  ) %>% 
  filter(campaign != "(not set)") %>% 
  mutate(source = tolower(source), campaign = tolower(campaign), medium=tolower(medium)) %>% 
  group_by(campaign) %>% 
  mutate(campaign_total = sum(sessions)) %>% 
  filter(campaign_total>1000) %>% 
  ggplot(
    aes(
      x = sessions,
      y = rate,
      color = source
    )
  )+
  geom_point()+
  theme_fivethirtyeight()+
  scale_color_stata()+
  scale_y_continuous(labels = percent)+
  scale_x_continuous(labels=comma)+
  ggtitle("Volume and Conversion by campaign type")+
  labs(color= "")
  
campaign_perf %>% 
  filter(sessions >= 50) %>%
  mutate(
    rate= total_conv/sessions
  ) %>% 
  filter(campaign != "(not set)") %>% 
  mutate(source = tolower(source), campaign = tolower(campaign), medium=tolower(medium)) %>% 
  group_by(campaign) %>% 
  mutate(campaign_total = sum(sessions)) %>% 
  filter(campaign_total>1000) %>%
  group_by(
    source
  ) %>% 
  e_chart(sessions) %>% 
  e_scatter(rate) %>% 
  e_tooltip(trigger ="item", e_tooltip_item_formatter(style ='percent'))
  
  

flow <- 
  GA_pull_discovery(
    metrics = c(
      "pageviews"
    ),
    dimensions = c(
      "pagePath",
      "previousPagePath"
    )
  ) %>% 
  mutate(
    type_1 = classifier_cc(page_path),
    prev_type = classifier_cc(previous_page_path)
  )




######### THIS IS MY THING #########
#source("ga_init.R")
library(googleAnalyticsR)
library(tidyverse)
library(janitor)
library(modeafun)
library(modeasecure)
library(ggthemes)
library(scales)
date_range <- c("2019-12-01", "2020-06-01")
GA_pull_discovery <- 
  modeasecure::GA_pull_factory(
    email = "analytics.team@modea.com",
    view_id = 15128437,
    dates = date_range
  )

all_users <- segment_ga4("all", segment_id = "gaid::-1")
multi_users <- segment_ga4("multi-users", segment_id = "gaid::ePs4kwPtTqWZeqzRlLnH1g")


#### TOP LANDING AREAS ####

# remove internal referrals
top_landings <- 
  GA_pull_discovery(
    metrics = c(
      "sessions",
      "bounces"
    ),
    dimensions = c(
      "landingPagePath"
    )
  ) %>% 
  mutate(
    type = classifier_cc(landing_page_path)
  )
top_landings %>% 
  group_by(type) %>% 
  summarise(t = sum(sessions)) %>% 
  filter(t > 15000 & type != "exit") %>% 
  ggplot(
    aes(
      x = reorder(type, t),
      y = t,
    )
  )+
  geom_col(fill= ggthemes_data$stata$colors$names$value[8])+
  scale_fill_stata()+
  labs(x = "", y = "sessions")+
  scale_y_continuous(labels =comma)+
  theme_fivethirtyeight()+
  #theme(axis.text.x = element_text(angle=40, hjust=1))+
  coord_flip()+
  ggtitle(
    "Where users enter the site"
  )


type_list <- 
  flow %>% 
  group_by(
    type_1
  ) %>% 
  summarise(
    n = sum(pageviews)
  ) %>% 
  arrange(desc(n)) %>% 
  filter(n > 10000)

######
flow <- 
  GA_pull_discovery(
    metrics = c(
      "pageviews"
    ),
    dimensions = c(
      "pagePath",
      "previousPagePath"
    )
  ) %>% 
  mutate(
    type_1 = classifier_cc(page_path),
    prev_type = classifier_cc(previous_page_path)
  )
type_list <- 
  flow %>% 
  group_by(
    type_1
  ) %>% 
  summarise(
    n = sum(pageviews)
  ) %>% 
  arrange(desc(n)) %>% 
  filter(n > 20000)

flow %>% 
  filter(prev_type != "entrance" ) %>% #& type_2 !="exit") %>% 
  group_by(
    type_1,
    prev_type,
  ) %>% 
  summarise(
    total = sum(pageviews)
  ) %>% 
  mutate(
    type_1 = 
      case_when(
        type_1 %in% type_list$type_1 ~ type_1,
        TRUE ~ "other"
      ),
    prev_type= 
      case_when(
        prev_type %in% type_list$type_1 ~ prev_type,
        TRUE ~ "other"
      )
  ) %>% 
  filter(total > 0) %>% 
  group_by(
    prev_type
  ) %>% 
  mutate(group_total = sum(total)) %>% 
  mutate(
    color = 
      case_when(
        type_1 == prev_type ~ "same",
        TRUE ~ type_1
      )
  ) %>% 
  ggplot(
    aes(
      x = reorder(prev_type, group_total),
      y = total,
      fill = color
    )
  )+
  geom_col(alpha = 0.9)+
  theme_fivethirtyeight()+
  scale_fill_stata()+
  labs(fill="")+
  coord_flip()+
  ggtitle("Where do people navigate to from top areas?")

e_data <- flow %>% 
  filter(prev_type != "entrance" ) %>% #& type_2 !="exit") %>% 
  group_by(
    type_1,
    prev_type,
  ) %>% 
  summarise(
    total = sum(pageviews)
  ) %>% 
  mutate(
    type_1 = 
      case_when(
        type_1 %in% type_list$type_1 ~ type_1,
        TRUE ~ "other"
      ),
    prev_type= 
      case_when(
        prev_type %in% type_list$type_1 ~ prev_type,
        TRUE ~ "other"
      )
  ) %>% 
  filter(total > 0) %>% 
  group_by(
    prev_type
  ) %>% 
  mutate(group_total = sum(total)) %>% 
  mutate(
    color = 
      case_when(
        type_1 == prev_type ~ "same",
        TRUE ~ type_1
      )
  )
library(echarts4r)
e_data %>%
  group_by(
    prev_type,color
  ) %>% 
  summarise(
    total = sum(total)
  ) %>% 
  group_by(color) %>% 
  e_chart(prev_type) %>% 
  e_bar(total, stack = "grp") %>% 
  e_flip_coords()
  


#### DEMOGRAPHICS: AGE AND GENDER ####

demographics <- 
  GA_pull_discovery(
    metrics = 
      c(
        "users"
      ),
    dimensions = 
      c(
        "userAgeBracket",
        "userGender",
        "deviceCategory"
      ),
  )




test <- rates %>% 
  week_format(date)

df %>%
  mutate(
    week = isoweek(date),
    year = year(date),
    month = month(date),
    week_index =
      case_when(# week counts roll over past 52 if >1 year
        month == 1 & week > 50 ~ week,
        year != min(year) ~ week + (max(week) * (year - min(year))),
        TRUE ~ week  # otherwise, make sure weeks at end of year are counted correctly.
      )
  ) %>%
  group_by(
    week_index
  ) %>%
  summarise(
    sessions = sum(sessions),
    date_index = max(date),
    days = n_distinct(date)
  ) %>% 
  ungroup()




view_id = 15128437 ## carilion external visits view id.
date_range <- c(Sys.Date() %m-% months(2), Sys.Date())

GA_pull_discovery <- modeasecure::GA_pull_factory(
  email = "analytics.team@modea.com",
  view_id = view_id,
  dates = date_range
)

## get all the relevant conversions from event data, and break it out by date and conversion type and pagepath.
conv_filt <- filter_clause_ga4(list(dim_filter("eventCategory", "EXACT", "Global Conversions")))
conversion <- 
  GA_pull_discovery(
    
  dimensions = c(
    "eventAction",
    "pagePath",
    "date"
  ),
  metrics = c(
    "totalEvents",
    "uniqueEvents"
  ),
  dim_filters = conv_filt
) %>% 
  mutate(
    type = classifier_cc(page_path) ### this is our classifer function to break out the pages into page types for easier summarization.
  ) %>% 
  group_by(
    type,
    event_action,
    date
  ) %>% 
  summarise(
    conversions = sum(total_events)
  ) %>% 
  pivot_wider(
    names_from = event_action,
    values_from = conversions,
    values_fill = 0
  )
hits <- 
  GA_pull_discovery(
    dimensions = c(
      "pagePath",
      "date"
    ),
    metrics = c(
      "pageviews",
      "uniquePageviews"
    ),
  ) %>% 
  mutate(
    type = classifier_cc(page_path)
  ) %>% 
  group_by(type, date) %>% 
  summarise(
    pv = sum(pageviews)
  ) %>% 
  group_by(type) %>% 
  mutate(group_total = sum(pv))
hits %>% 
  mutate(graph_type = if_else(group_total < 8000, "other", type)) %>%
  ggplot(
    aes(
      x = date,
      y = pv,
      fill= graph_type
    )
  )+
  geom_col()+
  #scale_fill_tableau()+
  theme_fivethirtyeight()+
  scale_y_continuous(labels = comma)+
  ggtitle("What areas of the site do people visit most?")+
  labs(fill="")

library(janitor)
rates <- merge(conversion, hits, by= c("date","type")) %>% clean_names() %>% filter(pv>0) %>% 
  mutate(rate = (click_to_call+make_appointment) / pv)

rates %>% 
  pivot_longer(
    cols = 3:5, 
    names_to = "conv_type", 
    values_to = "conversions"
    ) %>% 
  ggplot(
    aes(
      x = date,
      y = conversions,
      fill = conv_type
    )
  )+
  geom_col()+
  theme_modea()
library(ggthemes)
library(viridis)
rates %>% 
  mutate(
    total_conv = click_to_call+make_appointment
  ) %>% 
  filter(
    type %in% c("covid-19-vaccine", "locations", "providers", "mychart","homepage")
  ) %>% 
  ggplot(
    aes(
      x = date,
      y = total_conv,
      fill= type
    )
  )+
  geom_col()+
  scale_fill_few(palette = "Light")+
  theme_fivethirtyeight()+
  ggtitle("Where did users click for calls & appointments?")+
  labs(fill="")



# how long users take to convert on their session
conv_depth <- 
  GA_pull_discovery(
    dimensions = c(
      "eventAction",
      "pagePath",
      "date",
      "pageDepth"
    ),
    metrics = c(
      "totalEvents",
      "uniqueEvents"
    ),
    dim_filters = conv_filt
  ) %>% 
  mutate(
    type = 
      classifier_cc(page_path)
  ) %>% 
  group_by(
    event_action,
    type,
    page_depth
  ) %>% 
  summarise(
    sessions = unique_events
  )
a <- conv_depth %>% 
  mutate(page_depth = 
           case_when(
             as.numeric(page_depth) > 9 ~ 10,
             page_depth == "0" ~ 1,
             TRUE ~ as.numeric(page_depth)
           )
  ) %>%
  group_by(
    page_depth,
    event_action
  ) %>% 
  summarise(
    total = sum(sessions)
  ) %>% 
  group_by(event_action) %>% 
  mutate(
    per = round(total  /sum(total), 2)
  )
a %>% 
  ggplot(
    aes(
      x = as.numeric(page_depth),
      y = per,
      fill = event_action
    )
  )+
  geom_col(position="dodge")
           
conv_depth %>% 
  filter(type %in% c("covid-19-vaccine", "locations", "providers", "mychart","homepage") ) %>% 
  filter(as.integer(page_depth) < 20) %>% 
  ggplot(
    aes(
      x = as.integer(page_depth),
      fill = type
    )
  )+
    geom_density()+
  facet_grid(type~.)





discovery_temporal %>%
  mutate(
    week = isoweek(date),
    year = year(date),
    month = month(date),
    week_index =
      case_when(# week counts roll over past 52 if >1 year
        month == 1 & week > 50 ~ week,
        year != min(year) ~ week + (max(week) * (year - min(year))),
        TRUE ~ week  # otherwise, make sure weeks at end of year are counted correctly.
      )
  ) %>%
  group_by(
    week_index
  ) %>%
  summarise(
    sessions = sum(sessions),
    date_index = max(date),
    days = n_distinct(date)
  ) %>% 
  ungroup() %>%
  filter(days == 7) %>%
  ggplot(aes(date_index, sessions)) +
  geom_point(alpha = 0.7) +
  geom_line(alpha = 0.7) +
  #scale_x_date(date_labels = "%b %y") +
  scale_y_continuous(labels = comma) + # Make sure this scale looks good
  xlab("Date") +
  ylab("Weekly sessions") +
  scale_color_manual(values = modea_palette()) +
  theme_modea() +
  theme(legend.title = element_blank())




