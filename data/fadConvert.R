library(tidyverse)
library(DomoR)
library(statsr)
library(psych)
library(plotly)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(".httr-oauth")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]

ccid <- 15128437
fadSeg <- segment_ga4("fadusers", segment_id = "gaid::piHFzdn4SiqIhUR-UkUOhQ")
fadConvert <- segment_ga4("fadConverts", segment_id = "gaid::x2bO4k1NSFKIPS4I84JAZg")
totalSessions <- google_analytics(ccid, 
                              date_range = c("2017-01-01","2019-12-30"), 
                              metrics = c("sessions"
                              ), 
                              dimensions = c("date"
                              ),
                              anti_sample = TRUE)
fadSessions <- google_analytics(ccid, 
                                  date_range = c("2017-01-01","2019-12-30"), 
                                  metrics = c("sessions"
                                  ), 
                                  dimensions = c("date"
                                  ),
                                segments = fadSeg,
                                  anti_sample = TRUE)
fadConverts <- google_analytics(ccid, 
                                  date_range = c("2017-01-01","2019-12-30"), 
                                  metrics = c("sessions"
                                  ), 
                                  dimensions = c("date"
                                  ),
                                segments = fadConvert,
                                  anti_sample = TRUE)
fadConverts <- 
  fadConverts %>% 
  rename(
    conversions = sessions
  ) %>% 
  select(
    -segment
  )
fadSessions <- 
  fadSessions %>% 
  rename(
    fadViews = sessions
  ) %>% 
  select(
    -segment
  )



data <- merge(
  totalSessions, fadSessions, by = "date"
)
data <- merge(data, fadConverts, by = "date")

data <- 
  data %>% 
  mutate(
    rateTotal = conversions / sessions,
    rateStep = conversions / fadViews
  )
data %>% 
  mutate(
    period = 
      if_else(
        date > '2017-08-30',
        "post",
        "pre"
      )
  ) %>% 
  group_by(
    period
  ) %>% 
  summarise(
    sessions = sum(sessions),
    fadViews = sum(fadViews),
    conversions = sum(conversions)
  ) %>% 
  mutate(
    rateTotal = conversions / sessions,
    rateStep = conversions / fadViews
  )
  