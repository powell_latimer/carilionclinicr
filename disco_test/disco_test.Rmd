---
title: "Client Data Discovery"
author: "Modea Analysts"
output:
  modeafun::modea_html:
  modeafun::modea_pdf:
# Comment out the line below if using pdf
# knit: pagedown::chrome_print
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE)
```

```{r load-packages}
library(tidyverse)
library(modeafun)
library(modeasecure)
library(scales)
library(ggrepel)
library(glue)
library(lubridate)
library(kableExtra)
library(wordcloud)
```

```{r set-options}
# default date range is set 2 months from today
date_range <- c(Sys.Date() %m-% months(2), Sys.Date())

# Enter in client-specific information below
GA_pull_discovery <- modeasecure::GA_pull_factory(
  email = "analytics.team@modea.com",
  view_id = 15128437,
  dates = date_range
)

# There are multiple code chunks that are currently set to "include = false" below:
#   *landing-pages (a graph of landing pages categorized by page level)
#   *second-pages (a graph of second pages categorized by page level)
#   *search-terms (a wordcloud of site search terms)
```

#### `r format(date_range[1], "%B %d, %Y")` - `r format(date_range[2], "%B %d, %Y")` {.date}

```{r pull-data}
# Pull in data for the default report
discovery_sessions <- GA_pull_discovery(
  metrics = c(
    "sessions",
    "bounces",
    "avgSessionDuration",
    "pageviews"
  ),
  dimensions = c(
    "medium",
    "campaign",
    "source",
    "country",
    "region",
    "deviceCategory",
    "browser"
  )
)

discovery_pages <- GA_pull_discovery(
  metrics = c(
    "pageviews"
  ),
  dimensions = c(
    "userType",
    "pagePath"
  )
)

discovery_temporal <- GA_pull_discovery(
  metrics = c(
    "sessions"
  ),
  dimensions = c(
    "userType",
    "date",
    "dayOfWeek",
    "week"
  )
)

discovery_loadtime <- GA_pull_discovery(
  metrics = c(
    "avgPageLoadTime",
    "bounces",
    "sessions",
    "pageviews"
  ),
  dimensions = c(
    "deviceCategory",
    "browser"
  )
)

discovery_demographics <- GA_pull_discovery(
  metrics = c(
    "sessions"
  ),
  dimensions = c(
    "userAgeBracket",
    "userGender"
  )
)

discovery_landing <- GA_pull_discovery(
  dimensions = c(
    "landingPagePath",
    "secondPagePath"
  ),
  metrics = c(
    "sessions",
    "bounces"
  )
)

discovery_search <- GA_pull_discovery(
  dimensions = c("searchKeyword"),
  metrics = c("pageviews")
)
``` 

# Site Health: What is working?

The average load time across all pages is `r summarize(discovery_loadtime, round(sum(avg_page_load_time * pageviews) / sum(pageviews), 1))` seconds. Common performance metrics across all website pages are:  

``` {r metric-overivew}
discovery_sessions %>%
  kable_sessions()
```

### Traffic over time

```{r traffic-trend}
# Powell to add this code as function to package
discovery_temporal %>%
  mutate(
    week = isoweek(date),
    year = year(date),
    month = month(date),
    week_index =
      if_else(
        year != min(year),
        week + (52 * (year - min(year))), # week counts roll over if > 1 year
        if_else(
          week < 2 & month > 11,
          week + 52,
          week
        ) # otherwise, make sure weeks at end of year are counted correctly.
      )
  ) %>%
  group_by(
    week_index
  ) %>%
  summarise(
    sessions = sum(sessions),
    date_index = max(date),
    days = n_distinct(date)
  ) %>%
  ungroup() %>%
  filter( # get rid of weeks < 7 days (often first and last)
    days == 7
  ) %>%
  ggplot(aes(x = date_index, y = sessions)) +
  geom_point(color = modea_palette(1), alpha = 0.7) +
  geom_line(color = modea_palette(1), alpha = 0.7) +
  scale_x_date(date_labels = "%b %y") +
  scale_y_continuous(labels = comma) +
  xlab("Date") +
  ylab("Weekly sessions") +
  theme_modea()
```

### Browser usage  

```{r browser-plot}
browsers_to_plot <- discovery_sessions %>%
  reclassify_other(browser, reclassify_at = 8) %>%
  group_by(browser) %>%
  calculate_percent()

# Create a basic bar
ggplot(browsers_to_plot,
  aes(x = reorder(browser, percent), y = percent)
  ) +
  geom_bar(stat = "identity", color = "black", fill = modea_palette(1)) +
  theme_modea() +
  scale_y_continuous(labels = percent_format(1)) +
  ylab("% Sessions") +
  xlab("Browser") +
  coord_flip()
```

### Device usage  

```{r device-plot}
device_sum <- discovery_sessions %>%
  group_by(device_category) %>%
  calculate_percent()

# Create a basic bar
device.plot <- ggplot(
  device_sum,
  aes(x = "", y = percent, fill = device_category)
  ) +
  geom_bar(
    stat = "identity",
    width = 1,
    color = "black"
  )

# Convert to polar coordinates and add labels
device.plot <- device.plot +
  scale_fill_manual(values = modea_palette()) +
  coord_polar("y", start = 0) +
  geom_label(
    aes(label = paste0(round(percent * 100), "%")),
    position = position_stack(vjust = 0.5),
    color = "white",
    show.legend = FALSE
  )

# Tidy up the theme
device.plot +
  theme_modea() +
  labs(x = NULL, y = NULL, fill = NULL) +
  theme(
    axis.line = element_blank(),
    axis.text = element_blank(),
    axis.ticks = element_blank()
  )
```

```{r browser-device-performance-plot}
discovery_loadtime %>%
  mutate(total_load_time = avg_page_load_time * pageviews,
         device_and_browser = glue("{device_category}: {browser}")
         ) %>%
  reclassify_other(device_and_browser, reclassify_at = 0.02) %>%
  filter(device_and_browser != "Other") %>% 
  group_by(device_category, device_and_browser) %>% 
  summarize(
    avg_load_time = sum(total_load_time) / sum(pageviews),
    bounce_rate = sum(bounces) / sum(sessions),
    total_sessions = sum(sessions)
  ) %>%
  ggplot(aes(avg_load_time, bounce_rate, color = device_category)) +
  geom_point() +
  geom_label_repel(
    aes(label = device_and_browser,
        size = total_sessions)
  ) +
  scale_size(
    guide = "none",
    range = c(2, 5)
  ) +
  scale_y_continuous(
    labels = percent,
    limits = c(0.3, 1)
  ) +
  labs(
    title = "Site Performance by Browser and Device Type",
    color = "Device type"
  ) +
  xlab("Average page load time (seconds)") +
  ylab("Bounce rate") +
  scale_color_manual(values = modea_palette()) +
  theme_modea(legend_position = "bottom") +
  theme(axis.title = element_text(size = 16))
```
 
 
```{r search-terms, eval=FALSE, fig.height = 6, fig.width = 6}
# If this is used, add the header ### What do users search fwithin the site?
discovery_search <- discovery_search %>%
  mutate(
    keyword = str_to_lower(search_keyword) %>%
      str_trim() %>%
      str_trunc(side = "right", width = 35)
  ) %>%
  filter(str_length(keyword) >= 3) %>%
  group_by(keyword) %>%
  tally(pageviews)

wordcloud(
  words = discovery_search$keyword,
  freq = discovery_search$n,
  scale = c(3, 0.5),
  max.words = 100,
  random.order = FALSE,
  colors = c(modea_palette(3), modea_palette(2), modea_palette(1))
)
```


# Audience & Behavior: What does your audience look like?

### What is the age and gender breakdown?

```{r demographcics}
discovery_demographics %>%
  group_by(user_age_bracket, user_gender) %>%
  summarize(sessions = sum(sessions)) %>%
  ggplot(aes(user_age_bracket, sessions, fill = user_age_bracket)) +
  geom_col(color = "black") +
  facet_wrap(~user_gender) +
  xlab("Age") +
  ylab("Sessions") +
  scale_y_continuous(labels = comma) +
  scale_fill_manual(values = modea_palette()) +
  theme_modea("none")
```

### New versus returning users  

New users make up `r percent(sum(filter(discovery_temporal, user_type == "New Visitor")[, "sessions"]) / sum(discovery_temporal$sessions), 1)` of the total number of sessions.  

```{r new-users-table}
# This might be nicer as a plot over time
# with a summary statistic for the total number of users above...
discovery_temporal %>%
  mutate(
    week = isoweek(date),
    year = year(date),
    month = month(date),
    week_index =
      if_else( # week counts roll over past 52 if >1 year
        year != min(year),
        week + (52 * (year - min(year))),
        if_else(
          week < 2 & month > 11,
          week + 52,
          week
        ) # otherwise, make sure weeks at end of year are counted correctly.
      )
  ) %>%
  group_by(
    week_index, user_type
  ) %>%
  summarise(
    sessions = sum(sessions),
    date_index = max(date),
    days = n_distinct(date)
  ) %>%
  ungroup() %>%
  filter(days == 7) %>%
  ggplot(aes(date_index, sessions, color = user_type)) +
  geom_point(alpha = 0.7) +
  geom_line(alpha = 0.7) +
  scale_x_date(date_labels = "%b %y") +
  scale_y_continuous(labels = comma) + # Make sure this scale looks good
  xlab("Date") +
  ylab("Weekly sessions") +
  scale_color_manual(values = modea_palette()) +
  theme_modea() +
  theme(legend.title = element_blank())
```

### Location 

`r percent(sum(filter(discovery_sessions, country == "United States")[, "sessions"]) / sum(discovery_sessions$sessions), 1)` of sessions on the site occur within the United States. Of those sessions, the top states are:  

```{r location-table}
discovery_sessions %>%
  filter(country == "United States") %>%
  mutate(state = str_to_title(region)) %>%
  group_by(state) %>%
  kable_usage(num_rows = 6)
```

### Top pages by pageview

```{r top-pages-table}
# If you want to remove query params, add str_remove(page_path, "\\?.*")
discovery_pages %>%
  mutate(
    page_path = str_to_lower(page_path) %>%
      str_remove_all("\\?.*")
  ) %>%
  group_by(page_path) %>%
  kable_usage(
    num_rows = 50,
    frequency = pageviews
  ) %>%
  scroll_box(height = "300px")
```


```{r landing-pages, eval=FALSE}
# If this graph is used, include the heading ### Top landing pages

# Check this function to see what page level to use
# and if further term classification is necessary before including this part
# Be sure to change the homepage to some version of "homepage" if it is blank.
return_page_level(
  df = discovery_landing,
  page_path = landing_page_path
) %>%
  count(landing_page_level_1, landing_page_level_2, landing_page_level_3) %>%
  arrange(desc(n))

discovery_landing %>%
  return_page_level(landing_page_path, page_level = 2) %>%
  reclassify_other(landing_page_level_2, reclassify_at = 10) %>%
  group_by(landing_page_level_2) %>%
  summarize(sessions = sum(sessions)) %>%
  ggplot(aes(reorder(landing_page_level_2, sessions), sessions)) +
  geom_col(color = "black") +
  scale_y_continuous(labels = comma) +
  xlab("Landing page") +
  ylab("Sessions") +
  coord_flip() +
  theme_modea()
```

```{r second-pages, eval=FALSE}
# If this graph is used, include the heading ### Top second pages
return_page_level(
  df = discovery_landing,
  page_path = second_page_path,
  page_level = 2
) %>%
  reclassify_other(second_page_level_2, reclassify_at = 10) %>%
  group_by(second_page_level_2) %>%
  summarize(sessions = sum(sessions)) %>%
  ggplot(aes(reorder(second_page_level_2, sessions), sessions)) +
  geom_col(color = "black", fill = modea_palette(1)) +
  scale_y_continuous(labels = comma) +
  xlab("Landing page") +
  ylab("Sessions") +
  coord_flip() +
  theme_modea()
```

### Top sources and mediums  

```{r source-medium-table}
discovery_sessions %>%
  group_by(source, medium) %>%
  kable_usage(
    num_rows = 7
  )
```

```{r source-medium-plot}
discovery_sessions %>%
  mutate(
    source = recode(source, "(direct)" = "direct"),
    medium = recode(medium, "(none)" = "none")
  ) %>%
  group_by(source, medium) %>%
  summarize(
    total_sessions = sum(sessions),
    bounce_rate = sum(bounces) / sum(sessions)
  ) %>%
  ungroup() %>%
  arrange(desc(total_sessions)) %>%
  slice(1:7) %>%
  mutate(source_medium = glue("{medium}: {source}")) %>%
  ggplot(aes(total_sessions, bounce_rate, color = source_medium)) +
  geom_point() +
  geom_label_repel(aes(label = source_medium)) +
  theme_modea(legend_position = "none") +
  scale_x_continuous(labels = comma) + # change scales as necessary
  scale_y_continuous(
    labels = percent,
    limits = c(0.3, 1)
  ) +
  xlab("Sessions") +
  ylab("Bounce rate") +
  labs(title = "Traffic and Bounce Rate by Top Source/Medium ") +
  scale_color_manual(values = modea_palette())
```

# Discovery Questions

### What are the main call to actions?

*
* 
*

### What is presented on the main navigation?

*
*
*

### What are users searching for?

*
*
*

### What are the visible pain points?

*
*
*

# Campaign Performance

### What primary campaigns have run in the past 6 months?  

`r percent(sum(filter(discovery_sessions, campaign != "(Not set)")[, "sessions"]) / sum(discovery_sessions$sessions), 1)` of sessions were brought in by campaigns. Of those campaigns, the top performers were:  

```{r campaign-table}
# Often default campaign names are not descriptive.
# If this is the case, try to reclassify campaigns before producing tables
discovery_sessions %>%
  filter(campaign != "(not set)") %>%
  reclassify_other(campaign, reclassify_at = 0.01) %>%
  mutate(
    campaign = str_trunc(
      campaign,
      width = 40,
      side = "right"
    )
  ) %>%
  group_by(campaign) %>%
  kable_usage(num_rows = 8)
```

### Where are these campaigns taking place?   

```{r campaign-source-table}
discovery_sessions %>%
  filter(campaign != "(not set)") %>%
  mutate(campaign = str_trunc(
    campaign,
    width = 30,
    side = "right"
  )) %>%
  group_by(source, campaign) %>%
  kable_usage(num_rows = 8)
```
