#Scott Cooper's Script


library(tidyverse)
library(DomoR)
library(statsr)
library(psych)
library(plotly)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
library(robustbase)
### Google Analytics Init
ga_auth(".httr-oauth")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]

ccid <- 15128437
scottPages <- dim_filter(
  "pagePath", "REGEXP", "/fevar|/tcar|/skincare-laser|/be-fast|/weightloss|simcenter"
)
scottFilter <- filter_clause_ga4(
  list(
    scottPages
  )
)
scottEvents <- dim_filter(
  "eventCategory", "REGEXP", "Landing Page"
)
scottFilter2 <- filter_clause_ga4(
  list(
    scottPages,
    scottEvents
  ),
  "AND"
)
scottpv <- google_analytics(ccid, 
                         date_range = c("2019-03-01","2019-07-30"), 
                         metrics = c("pageviews", 
                                     "uniquePageviews",
                                     "avgTimeOnPage",
                                     "bounceRate",
                                     "entrances"
                         ), 
                         dimensions = c("pagePath"
                         ),
                         dim_filters = scottFilter,
                         anti_sample = TRUE
                         )
scottconv <- google_analytics(ccid, 
                            date_range = c("2019-03-01","2019-07-30"), 
                            metrics = c("totalEvents", 
                                        "uniqueEvents"
                            ), 
                            dimensions = c("pagePath",
                                           "eventCategory",
                                           "eventAction"
                            ),
                            dim_filters = scottFilter2,
                            anti_sample = TRUE
)
scottData <- left_join(scottpv, scottconv, by = "pagePath") %>% 
  filter(pageviews > 5) %>% 
  group_by(pagePath) %>% 
  mutate(totalConversions = sum(totalEvents),
         conversionRate = totalConversions / pageviews,
         entranceRate = entrances / pageviews)

write.csv(scottData, file = "scottData.csv")

