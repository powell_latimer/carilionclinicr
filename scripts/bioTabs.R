## search converts
library(tidyverse)
library(DomoR)
library(statsr)
library(psych)
library(plotly)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(".httr-oauth")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]
#set id
ccid <- 15128437
# create filter object for global search queries and click throughs
eventFilt <- filter_clause_ga4(list(dim_filter("eventAction", "REGEX", "Menu Tab Clicks")))

#  pull in 
cc_bios <- 
  google_analytics(ccid, 
                   date_range = c("2019-08-01","2019-12-31"), 
                   metrics = c("totalEvents",
                               "uniqueEvents"
                   ), 
                   dimensions = c("eventAction",
                                  "eventLabel",
                                  "pagePath"
                   ),
                   dim_filters = eventFilt,
                   anti_sample = TRUE)

bioList <- 
  cc_bios %>% 
  mutate(
    eventLabel = tolower(eventLabel)
  ) %>% 
  mutate(
    ind = 
      if_else(
        eventLabel == "biography" |eventLabel == "biograpfia",
        TRUE,
        FALSE
    )
  ) %>% 
group_by(
  pagePath
) %>% 
  mutate(
    bioclicks = sum(ind)
  ) %>% 
  filter(
    bioclicks > 0
  )

bioList <- 
  bioList %>% 
  filter(
    eventLabel != "(not set)"
  )
library(devtools)
install_github("cttobin/ggthemr")
library(ggthemr)
ggthemr::ggthemr(palette = "fresh", layout = "minimal")
install.packages("wesanderson")
library(wesanderson)
bioList %>% 
  mutate(
    eventLabel = 
      if_else(
        eventLabel == "biografia", "biography",
        if_else(
          eventLabel == "especialidades", "specialties",
          eventLabel
        )
      )
  ) %>% 
  filter(
    eventLabel != "videos"
  ) %>% 
  group_by(
    eventLabel
  ) %>% 
  summarise(
    totalEvents = sum(totalEvents)
  ) %>% 
  ggplot(
    aes(
      x = reorder(eventLabel, -totalEvents),
      y = totalEvents
    )
  )+
  geom_col(
    aes(
      fill = reorder(eventLabel, -totalEvents)
    )
  )+
  scale_y_continuous(labels = comma)+
  theme_minimal()+
  theme(axis.text.x = element_text(angle = 60, hjust =1))+
  labs(
    x = "",
    y = "Total Events",
    fill = "Menu Tab"
  )+
  theme(
    text = element_text(family = "serif")
  )
  













  
