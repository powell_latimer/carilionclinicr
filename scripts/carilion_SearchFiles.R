## search converts
library(tidyverse)
library(DomoR)
library(statsr)
library(psych)
library(plotly)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(".httr-oauth")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]
#set id
ccid <- 15128437
# create filter object for global search queries and click throughs
convertFilt <- filter_clause_ga4(list(dim_filter("eventCategory", "REGEX", "Card Click")))
searchFilt <- filter_clause_ga4(list(dim_filter("eventAction", "REGEX", "Query|Autocomplete",)))

#  pull in 
rawSearch <- 
  google_analytics(ccid, 
                   date_range = c("2019-01-01","2019-9-15"), 
                   metrics = c("totalEvents",
                               "uniqueEvents"
                               ), 
                   dimensions = c("dimension2",
                                  "eventLabel",
                                  "dateHourMinute"
                                  ),
                   dim_filters = searchFilt,
                   anti_sample = TRUE)
rawConverts <- 
  google_analytics(ccid, 
                   date_range = c("2019-01-01","2019-9-15"), 
                   metrics = c("totalEvents",
                               "uniqueEvents"
                   ), 
                   dimensions = c("dimension2",
                                  "eventCategory",
                                  "eventAction",
                                  "eventLabel",
                                  "dateHourMinute"
                   ),
                   dim_filters = convertFilt,
                   anti_sample = TRUE)


# data formatting ---------------------------------------------------------

# take the raw search terms and clean out the unusable  terms
search <- rawSearch %>% 
  filter(
    !str_length(eventLabel) < 3
    ) %>% 
  # make a lowercase eventLabel
  mutate(
    eventLabel = tolower(eventLabel),
    eventLabel = str_remove_all(eventLabel, "[:punct:]")
  ) %>% 
  # identify earliest search query
  group_by(
    dimension2
  ) %>% 
  mutate(
    index = row_number(eventLabel),
    firstSearch = min(dateHourMinute),
    fullTerm = eventLabel[index == max(index)]
  ) %>% 
  ungroup() %>% 
  # identify last search term and earliest query, and summarise data by those metrics.
  group_by(
    dimension2,
    firstSearch,
    fullTerm
  ) %>% 
  summarise(
    totalQueries = sum(totalEvents)
  )
  
  
# clean your convert dataset
converts <- 
  rawConverts %>% 
  select(
    dimension2,
    eventCategory,
    dateHourMinute
  ) %>% 
  unique() %>% 
  rename(
    clickType = eventCategory,
    clickTime = dateHourMinute
  )

data <- 
  left_join(
    search,
    converts, by =
      "dimension2"
  ) %>% 
  ungroup() %>% 
  mutate(
    clickTime = ymd_hm(clickTime),
    firstSearch = ymd_hm(firstSearch),
    clickAfterSearch = if_else(clickTime >= firstSearch, T, F) 
  )

  


