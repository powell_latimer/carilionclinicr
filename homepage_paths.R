library(modeafun)
library(tidyverse)
library(DomoR)
library(statsr)
library(psych)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(email="powell.latimer@modea.com")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]

ccid <- 15128437
date_range = c("2020-08-01","2020-12-30")
home_filt <- 
  filter_clause_ga4(
    list(
      dim_filter(
        "landingPagePath", "EXACT", "/"
      )
    )
  )
cc_home <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("totalEvents", "uniqueEvents"), 
                   dimensions = c("landingPagePath",
                                  "pagePath",
                                  "dimension2",
                                  "dateHourMinute",
                                  "eventCategory",
                                  "eventLabel",
                                  "eventAction",
                                  "source",
                                  "medium"),
                   dim_filters = home_filt,
                   anti_sample = TRUE) %>% 
  filter(
    eventCategory == "Client ID"
  )

top_paths <-
  cc_home %>% 
  group_by(
    pagePath,
    medium
  ) %>% 
  summarise(
    total = sum(uniqueEvents)
  ) %>% 
  mutate(
    pagePath = str_remove(pagePath, "/"),
    pagePath = paste0(pagePath, "/"),
    page_type = 
      substr(pagePath, 0, str_locate(pagePath, "/"))
  ) %>% 
  group_by(
    page_type
  ) %>% 
  mutate(
    group_total = sum(total)
  )
list <-
  top_paths %>% 
  group_by(
    page_type
  ) %>% 
  summarise(
    group_total = sum(total)
  ) %>% 
  filter(group_total > 250)
library(ggthemes)
top_paths %>% 
  mutate(
    page_type = if_else(page_type %in% list$page_type, page_type, "other")
  ) %>% 
  filter(page_type != "other") %>% 
  group_by(
    page_type,
    medium
  ) %>% 
  summarise(
    group_total = sum(total)
  ) %>% 
  mutate(
    per = group_total/sum(group_total),
    medium = if_else(medium=='(none)', "direct", medium)
  ) %>% 
  filter(page_type != "/") %>%
  filter(group_total > 50) %>% 
  ggplot(
    aes(
      x = reorder(page_type, group_total),
      y = group_total
    )
  )+
  geom_col(aes(fill = medium))+
  coord_flip()+
  theme_fivethirtyeight()+
  scale_fill_tableau()+
  ggtitle(
    "Where do sessions that land on home go?",
    subtitle = "and where do they come from?"
  )
df <- top_paths %>% 
  mutate(
    page_type = if_else(page_type %in% list$page_type, page_type, "other")
  ) %>% 
  filter(page_type != "other") %>% 
  group_by(
    page_type,
    medium
  ) %>% 
  summarise(
    group_total = sum(total)
  ) %>% 
  mutate(
    per = round(group_total/sum(group_total),4),
    medium = if_else(medium=='(none)', "direct", medium)
  ) 
df_sum <-
  df %>% 
  group_by(
    medium
  ) %>% 
  summarise(
    mean = weighted.mean(per, group_total)
  )

sources <-
  cc_home %>% 
  filter(medium == "referral") %>% 
  filter(
    str_detect(pagePath, "locations|providers|specialties")
  ) %>% 
  group_by(
    source
  ) %>% 
  summarise(
    total = sum(uniqueEvents)
  )
