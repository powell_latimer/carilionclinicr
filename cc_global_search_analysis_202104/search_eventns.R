### what do users click through from global search?
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(email = "analytics.team@modea.com")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
library(tidyverse)
library(stringr)
library(janitor)
# set vars
ccid <- 15128437
range = c("2020-03-01", "2021-04-14")
filt <- filter_clause_ga4(list(dim_filter("pagePath", "REGEX", "/search|^/specialties$|^/locations$|^/providers$")))
seg <- segment_ga4("searchers", segment_id = "gaid::Wwi9E1KGRRek_QQWkZwlCg")
#### total success rate for all search tools 
all_clickthroughs <- #ts_df for timestamp dataframe.
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("totalEvents", "uniqueEvents" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     "pagePath", # page path, 
                     "eventCategory", 
                     "eventAction", 
                     "eventLabel", # this is your query term
                     "date" ,
                     "dimension2"
                   ),
                   dim_filters = filt, # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names() %>% 
  filter(
    str_detect(event_category, "Card Click|Clickthrough")
  ) %>% 
  mutate(
    event_category = if_else(page_path == "/specialties", "Specialty Card Click", event_category) 
  ) %>% 
  mutate(
    search_tool = 
      case_when(
        str_detect(page_path, "/locations$") ~ "location search",
        str_detect(page_path, "/providers$") ~ "provider search",
        str_detect(page_path, "/specialties$") ~ "specialty search",
        TRUE ~ "global search"
      )
  )

search_traffic <- #ts_df for timestamp dataframe.
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("pageviews" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     "pagePath", # page path,  # this is your query term
                     "date",
                     "dimension2"
                   ),
                   dim_filters = filt, # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names() %>% 
  mutate(
    search_tool = 
      case_when(
        str_detect(page_path, "/locations$") ~ "location search",
        str_detect(page_path, "/providers$") ~ "provider search",
        str_detect(page_path, "/specialties$") ~ "specialty search",
        TRUE ~ "global search"
      )
  )
p_filt <- filter_clause_ga4(list(dim_filter("previousPagePath", "REGEX", "/search")))
search_previous <- #ts_df for timestamp dataframe.
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("pageviews" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     "pagePath",
                     "previousPagePath", # page path,  # this is your query term
                     "date",
                     "dimension2"
                   ),
                   dim_filters = p_filt, # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names() %>% 
  filter(
    !str_detect(page_path, "^/$|^/specialties$|^/locations$|^/providers$|/search")
  )
other_global_success <- 
  search_previous %>% 
  filter(date >= "2020-10-01") %>% 
  filter(previous_page_path == "/search") %>%
  filter(!str_detect(page_path, "locations/|specialties/|providers/")) %>% 
  group_by(
    date
    ) %>% 
  summarise(
    global_success = n_distinct(dimension2)
  ) %>% 
  mutate(
    search_tool = "global search"
  )


ct <- all_clickthroughs %>% group_by(date) %>% summarise(u_clicks = n_distinct(dimension2))
pv <- search_traffic %>% group_by(date) %>% summarise(traffic = n_distinct(dimension2))
a <- left_join(pv, ct, by = c("date"))
holistic <- 
  left_join(a, other_global_success, by = "date") %>% 
  mutate(
    tot_success = u_clicks+global_success,
    tot_success = if_else(is.na(tot_success), u_clicks, tot_success),
    rate = tot_success / traffic
    )

g_pv <- search_traffic %>% filter(page_path  == "/search") 
g_ct <- 
  all_clickthroughs %>% 
  filter(dimension2 %in% g_pv$dimension2) %>%
  mutate(
    range = if_else(date < "2020-10-01", TRUE, FALSE),
    ex = if_else(page_path != "/search", T, F),
    ind = range+ex
  ) %>% 
  filter(
    ind < 2
  ) %>% 
  group_by(date) %>% 
  summarise(u_clicks = n_distinct(dimension2))
g_pv <- g_pv %>% group_by(date) %>% summarise(traffic = n_distinct(dimension2))
g <- left_join(g_pv, g_ct, by = "date")
g_hol <- left_join(g, other_global_success, by = "date") %>% 
  mutate(
    tot_success = u_clicks+global_success,
    tot_success = if_else(is.na(tot_success), u_clicks, tot_success),
    rate = tot_success / traffic,
  )

g_hol %>% 
  ggplot(
    aes(
      x= date,
      y = rate,
    )
  )+
  geom_jitter(color = "midnightblue")+
  geom_smooth(method = "loess", color = 'firebrick')+
  theme_fivethirtyeight()+
  scale_y_continuous(limits = c(0,1), labels=percent)+
  ggtitle("Global Search Success Rate")

g_hol %>% 
  ggplot(
    aes(
      x= date,
      y = rate,
    )
  )+
  geom_jitter(color = "midnightblue")+
  geom_smooth(method = "loess", color = 'firebrick')+
  theme_fivethirtyeight()+
  scale_y_continuous(limits = c(0,1), labels=percent)+
  ggtitle("Global Search Success Rate")

g_hol %>% 
  mutate(
    month = month(date),
    period = if_else(date>="2020-10-01", "Post", "Pre")
  ) %>% 
  group_by(
    period,
    month,
  ) %>% 
  summarise(
    rate= weighted.mean(rate, traffic, na.rm = TRUE)
  )
g_hol %>% 
  ggplot(
    aes(
      x= date,
      y = traffic,
    )
  )+
  geom_jitter(color = "midnightblue")+
  geom_smooth(method = "loess", color = 'firebrick')+
  theme_fivethirtyeight()+
  ggtitle("Global Search Daily Usage (sessions)")

library(scales)
holistic %>% 
  mutate(month = month(date)) %>% 
  ggplot(
    aes(
      x= date,
      y = rate
    )
  )+
  geom_point()+
  geom_smooth(method = "glm")+
  theme_fivethirtyeight()+
  scale_y_continuous(labels=percent, limits = c(0, 1))+
  geom_vline(xintercept = as.Date("2020-09-01"), linetype = "dotted")


ng_success <- 
  all_clickthroughs %>% 
  filter(search_tool != "global search") %>% 
  group_by(
    date,
    search_tool
  ) %>% 
  summarise(
    successes = n_distinct(dimension2)
  )

ng_traffic <- 
  search_traffic %>% 
  filter(search_tool != "global search") %>% 
  group_by(date, search_tool) %>% 
  summarise(traffic = n_distinct(dimension2))

ng_total <- left_join(ng_traffic, ng_success, by = c("date", "search_tool")) %>% mutate(rate = successes / traffic)

ng_total %>% 
  ungroup %>% 
  mutate(week = as.integer(round((date-min(date)) / 7, 0))) %>% 
  group_by(week, search_tool) %>% 
  summarise(
    traffic = sum(traffic, na.rm = TRUE),
    rate = sum(successes) / sum(traffic)
  ) %>% 
  ggplot(
    aes(
      x = week,
      y = traffic,
      color = search_tool
    )
  )+
  geom_smooth(method = 'glm')+
  geom_jitter()+
  theme_fivethirtyeight()+
  labs(color = "Search Tool")+
  #scale_y_continuous(labels=percent, limits= c(0,1))+
  #geom_vline(xintercept = as.Date("2020-09-01"), linetype = "dotted")+
  ggtitle("CC.org search tool success rate")

holistic %>% 
  mutate(month = month(date)) %>% 
#  filter(month < 11) %>% 
  ggplot(
    aes(
      x = date,
      y = rate,
      #color = search_tool
    )
  )+
  geom_point()+
  geom_smooth(method = "glm")+
  scale_y_continuous(limits = c(0, 1),labels= percent)+
  theme_fivethirtyeight()+
  ggtitle("Search Success rate")
  


#### global search success rate
global_search_sessions <- 
  search_traffic %>% 
  filter(search_tool == "global search") %>% 
  rename(started = search_tool) %>% 
  select(dimension2, date, started)

global_total_ct <- all_clickthroughs %>% 
  filter(dimension2 %in% global_search_sessions$dimension2)

global_search_perf <- 
  left_join(global_search_sessions, global_total_ct, by = c("dimension2", "date")) %>% 
  group_by(dimension2) %>% 
  mutate(success = if_else(sum(unique_events, na.rm = T)>0, T,F)) %>% 
  mutate(
    success = if_else(success == F & dimension2 %in% search_previous$dimension2, T, success)
  ) %>% 
  group_by(date, success)%>% 
  summarise(
    total = n_distinct(dimension2)
  ) %>% 
  group_by(
    date
  ) %>% 
  mutate(
    rate = total / sum(total, na.rm= T)
  )

global_search_perf %>% 
  filter(success == T) %>% 
  ggplot(
    aes(
      x = date,
      y = rate,
      fill= success
    )
  )+
  geom_point()+
  geom_smooth()+
  scale_y_continuous(limits= c(0,1))

global_search_perf %>% 
  filter(success == T) %>% 
  ggplot(
    aes(
      x = total,
      y = rate,
    )
  )+
  geom_point()

## check query terms in November and December
q_filt <- filter_clause_ga4(list(dim_filter("eventAction", "REGEX", "Autocomplete|Query")))
queries_all <- #ts_df for timestamp dataframe.
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("totalEvents", "uniqueEvents" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     "pagePath",
                     "eventAction",
                     "eventLabel", # page path,  # this is your query term
                     "date",
                     "dimension2"
                   ),
                   dim_filters = q_filt, # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names() %>% 
  group_by(
    dimension2,
    event_action
  ) %>% 
  filter(
    str_length(event_label) == max(str_length(event_label))
  )

t <- 
  queries_all %>% 
  mutate(event_label = tolower(event_label)) %>% 
  mutate(
    month = month(date)
  ) %>% 
  group_by(
    month,
    event_label
  ) %>% 
  summarise(
    queries = sum(total_events)
  )


  

all_traffic <-
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("sessions" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     "date"
                   ), # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names()
  
search_sessions  <- search_traffic %>% group_by(date) %>% summarise(search_sessions = n_distinct(dimension2))
usage <- left_join(all_traffic, search_sessions, by = "date") %>% mutate(usage_rate = search_sessions/sessions)

usage %>% 
  ggplot(
    aes(
      x = date,
      y = usage_rate
    )
  )+
  geom_point()+
  geom_smooth(method = "glm")+
  theme_fivethirtyeight()+
  scale_y_continuous(labels=percent)+
  labs(x = "")+
  ggtitle("Usage rate of CC.org search tools")

eoy_queries <- 
  t %>% 
  filter(month >= 11) %>% 
  arrange(desc(queries)) 


c_filt <- filter_clause_ga4(list(dim_filter("eventCategory", "EXACT", "Global Conversions")))

converters <- #ts_df for timestamp dataframe.
  google_analytics(ccid, 
                   date_range = range, # 2-3 weeks is PLENTY
                   metrics = c("totalEvents", "uniqueEvents" # give me all the events
                   ), 
                   dimensions = c( # grab session id from dimension index list
                     # page path,  # this is your query term
                     "date",
                     "dimension2"
                   ),
                   dim_filters = c_filt, # give me ONLY the events that fire as client and session id. 
                   anti_sample = TRUE) %>% 
  clean_names()


c <- search_traffic %>% 
  mutate(conversion = if_else(dimension2 %in% converters$dimension2, TRUE, FALSE))
c %>% 
  group_by(
    search_tool
  ) %>% 
  summarise(n())
levels(c$search_tool) <- c("location search", "provider search", "specialty search", "global search")
relevel(c$search_tool, c("location search", "provider search", "specialty search", "global search"))
c_gg <- 
  c %>% 
  mutate(
    week = round((date-min(date)) / 7, 0)+1
  ) %>% 
  group_by(
    date,
    search_tool,
    conversion
  ) %>% 
  summarise(
    total = n_distinct(dimension2)
  ) %>% 
  group_by(date, search_tool) %>% 
  mutate(
    rate = total / sum(total)
  ) %>% 
  filter(conversion == T)

levels(c_gg$search_tool) <- c("location search", "provider search", "specialty search", "global search")

c_gg %>% 
  ggplot(
    aes(
      x = date,
      y = rate,
      color = search_tool
    )
  )+
  geom_smooth(method = "glm")+
  theme_fivethirtyeight()+
  scale_y_continuous(labels=percent)+
  labs(color = "Search Tool")+
  ggtitle("Conversion Rates among search users")+
  scale_color_stata()


queries <- 
  queries_all %>% 
  mutate(
    event_label = tolower(event_label)
  ) %>% 
  ungroup() %>% 
  mutate(
    month = substr(date, 0, 7),
    success = if_else(dimension2 %in% all_clickthroughs$dimension2 | dimension2 %in% other_global_success$dimension2, TRUE, FALSE),
  ) %>% 
  group_by(
    month,
    event_label,
    success
  ) %>% 
  summarise(
    total = n_distinct(dimension2)
  ) %>% 
  pivot_wider(
    names_from = success,
    values_from = total, 
    values_fill = 0
  ) %>% 
  mutate(
    rate = `TRUE` / (`TRUE`+`FALSE`),
    queries = `TRUE`+`FALSE`
  ) %>% 
  filter(event_label != "(not set)") %>% 
  rename(
    query = event_label,
    success = `TRUE`,
    fail = `FALSE`
  )

queries %>% 
  filter(queries >= 35) %>% 
  ggplot(
    aes(
      x = month, 
      y = rate,
      color = queries,
      size = queries
    )
  )+
  geom_text(aes(label = query))+
  scale_color_gradient(high = "darkorange", low = "purple4")+
  theme_fivethirtyeight()+
  guides(size=FALSE)+
  theme(axis.text.x = element_text(size=6))+
  scale_y_continuous(labels=percent)+
  ggtitle("Query terms by success rate")

  
queries %>% 
  filter(queries >= 35) %>% 
  ggplot(
    aes(
      x = month, 
      y = rate,
      color = queries,
      size = queries
    )
  )+
  geom_jitter()+
  scale_color_gradient(high = "darkorange", low = "purple4")+
  theme_fivethirtyeight()+
  guides(size=FALSE)+
  theme(axis.text.x = element_text(size=6))+
  geom_hline(yintercept = 0.5, linetype = "dotted")+
  scale_y_continuous(labels=percent)+
  ggtitle("Query terms by success rate")

queries %>% 
  filter(str_detect(query, "covid")) %>% 
  group_by(
    month
  ) %>% 
  summarise(
    queries = sum(queries),
    success = sum(success)
  ) %>% 
  mutate(
    rate = success / queries
  ) %>% 
  ungroup() %>% 
  ggplot(
    aes(
      x = month, 
      y = rate,
      size = queries,
    )
  )+
  geom_point()+
  theme_fivethirtyeight()+
  guides(size=FALSE)+
  theme(axis.text.x = element_text(size=6))+
  scale_y_continuous(limits = c(0,1))+
  ggtitle("Success rate of covid-related queries")
  
