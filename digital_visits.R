#-------------------------------#
##### under construction #######
#-------------------------------#

library(modeafun)
library(tidyverse)
library(statsr)
library(lubridate)
library(stringi)
library(scales)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(email="powell.latimer@modea.com")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]

ccid <- 15128437
date_range = c("2020-01-01","2020-12-30")
vv_filt <- 
  filter_clause_ga4(
    list(
      dim_filter(
        "pagePath", "REGEX", "digital-health"
      )
    )
  )
vv <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("pageviews", "uniquePageviews", "entrances", "bounces", "avgTimeOnPage"), 
                   dimensions = c("date"),
                   dim_filters = vv_filt,
                   anti_sample = TRUE) %>% 
  mutate(
    bounce_rate = bounces / entrances,
    per = uniquePageviews / sum(uniquePageviews)
  )
vv_date <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("pageviews", "uniquePageviews", "entrances", "bounces", "avgTimeOnPage"), 
                   dimensions = c("pagePath", "date"),
                   dim_filters = vv_filt,
                   anti_sample = TRUE) %>% 
  mutate(
    bounce_rate = bounces / entrances,
    per = uniquePageviews / sum(uniquePageviews)
  )
vv_date %>% 
  ggplot(
    aes(
      x = date,
      y = avgTimeOnPage
    )
  )+
  geom_point()
vv_date %>% 
  filter(pageviews > 50) %>% 
  mutate(
    period = if_else(date >= "2020-07-01", "LATE", "EARLY")
  ) %>%
  group_by(period) %>% 
  summarise(
    mean = mean(avgTimeOnPage)
  )
visits_seg <- 
  segment_ga4("all", segment_id = "gaid::A7LBvKY-TEu4NeSVhey6yw")
converts_seg <-
  segment_ga4(
    "converts", segment_id = "gaid::mzjcN1pHSSmpDafAulgsCQ"
  )
sessions_all  <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("sessions", "bounces"), 
                   dimensions = c("date"),
                   segments = visits_seg,
                   anti_sample = TRUE)
sessions_convert <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("sessions"), 
                   dimensions = c("date"),
                   segments = converts_seg,
                   anti_sample = TRUE) %>% 
  rename(
    conversions = sessions
  ) %>% 
  select(-segment)

sessions <-
  left_join(
    sessions_all, sessions_convert, by = "date"
  ) %>% 
  mutate(
    non_bounce = sessions-bounces,
    convert_rate = conversions / sessions
  )
  
sessions %>% 
  ggplot(
    aes(
      x = date,
      y = convert_rate
    )
  )+
  geom_point()

bounces <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("bounceRate"), 
                   dimensions = c("date"),
                   anti_sample = TRUE)
  
bounces %>% 
  filter(bounceRate > 1) %>% 
  summarise(
   mu = mean(bounceRate),
   hi = mean(bounceRate) + (1.6*(sd(bounceRate))),
   lo = mean(bounceRate) - (1.6*(sd(bounceRate)))
  )

events <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("totalEvents", "uniqueEvents"), 
                   dimensions = c("pagePath", "eventCategory", "eventAction", "eventLabel", "date"),
                   dim_filters = vv_filt,
                   anti_sample = TRUE) %>% 
  mutate(eventLabel = tolower(eventLabel)) %>% 
  filter(eventCategory == "Generic Link Clicks" | eventCategory == "YouTube") %>% 
  filter(
    str_detect(eventLabel, "vidyo|download|the app|connecting to care|patient guide")
  ) %>% 
  filter(
    !str_detect(eventAction, "Reached") 
  ) %>% 
  mutate(
    eventLabel =
      case_when(
        eventLabel == "download" | eventLabel == "the app" | eventLabel == "download the app" ~ "download the app", 
        TRUE ~ eventLabel
      )
  )
events %>% 
  filter(eventLabel != "explore the app") %>% 
  group_by(
    eventLabel
  ) %>% 
  summarise(
    total = sum(totalEvents)
  ) %>% 
  ggplot(
    aes(
      x = reorder(eventLabel, total),
      y = total,
      fill = eventLabel
    )
  )+
  geom_col()+
  theme_fivethirtyeight()+
  scale_fill_few()+
  theme(axis.text.x = element_blank())


vv_events <- 
  events %>% 
  group_by(
    eventCategory,
    date
  ) %>% 
  summarise(
    unique_ev = sum(uniqueEvents)
  ) %>% 
  spread(
    eventCategory, unique_ev, fill = 0
  )

entrance_filt <- 
  filter_clause_ga4(
    list(
      dim_filter(
        "landingPagePath", "REGEX", "digital-health"
      )
    )
  )

vv_acq<- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("sessions", "bounces"), 
                   dimensions = c("medium", "source", "campaign"),
                   dim_filters = entrance_filt,
                   anti_sample = TRUE) %>% 
  mutate(
    bounce_rate = bounces/sessions 
  )
### make viz
vv_acq %>% 
  group_by(
    campaign
  ) %>% 
  filter(
    sessions > 100
  ) #%>% 
  ggplot(
    aes(
      x = sessions,
      y = bounce_rate
    )
  )+
  geom_point()
library(ggthemes)
vv_date %>% 
  ggplot(
    aes(
      x = date,
      y = bounce_rate,
      )
  )+
  geom_point()+
  geom_smooth()+
  geom_hline(yintercept = 0.48)+
  geom_hline(yintercept = 0.41, linetype ="dotted")+
  geom_hline(yintercept = 0.54, linetype = "dotted")+
  scale_color_few()+
  theme_few()

convert_trend %>% 
  ggplot(
    aes(
      x = date,
      y = convert_click,
    )
  )+
  geom_point()+
  geom_smooth()+
  scale_color_few()+
  theme_few()

sessions %>% 
  filter(!is.na(convert_rate) & convert_rate < 0.8) %>% 
  ggplot(
    aes(
      x = date,
      y = convert_rate
    )
  )+
  geom_point()+
  theme_fivethirtyeight()+
  scale_y_continuous(labels = percent)+
  ggtitle(
    "Daily Conversion Rate"
  )

test <-
  left_join(
    sessions,
  vv_events, 
  by = "date"
  )




### dig deeper into 

events_source <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("totalEvents", "uniqueEvents"), 
                   dimensions = c("pagePath", "eventCategory", "eventAction", "eventLabel", "campaign"),
                   dim_filters = vv_filt,
                   anti_sample = TRUE) %>% 
  mutate(eventLabel = tolower(eventLabel)) %>% 
  filter(eventCategory == "Generic Link Clicks") %>% 
  filter(
    str_detect(eventLabel, "vidyo|download|the app|connecting to care|patient guide")
  ) %>% 
  mutate(
    eventLabel =
      case_when(
        eventLabel == "download" | eventLabel == "the app" | eventLabel == "download the app" ~ "download the app", 
        TRUE ~ eventLabel
      )
  ) %>% 
  mutate(
    campaign = if_else(campaign == "(not set)", "none", "campaign")
  )
events_source %>% 
  group_by(
    eventLabel,
    campaign
  ) %>% 
  summarise(
    total = sum(totalEvents)
  ) %>% 
  group_by(
    campaign
  ) %>% 
  mutate(per = percent(total / sum(total)))
  



