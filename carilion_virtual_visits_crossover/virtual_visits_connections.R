library(modeafun)
library(tidyverse)
library(statsr)
library(lubridate)
library(stringi)
library(scales)
library(ggthemes)
my_client_id <- "775449874495-f8e5s0rmpmf9g57qgc2h0u13kj852n3s.apps.googleusercontent.com"
my_client_secret <- "n5UgHMcJcGv1EQgPXUed8Sir"
options(googleAuthR.client_id = my_client_id)
options(googleAuthR.client_secret = my_client_secret)
library(googleAnalyticsR)
library(googleAuthR)
### Google Analytics Init
ga_auth(email="analytics.team@modea.com")
myaccounts <- ga_account_list()
segments <- ga_segment_list()
segmentList <- segments[["items"]]
segment <- segment_ga4("virtual", segment_id = "gaid::u8BTCLB6TbmDGFveYvPSAg")


ccid <- 15128437
date_range = c("2020-06-01","2021-01-30")
vv_filt <- 
  filter_clause_ga4(
    list(
      dim_filter(
        "pagePath", "REGEX", "digital-health"
      )
    )
  )
df <- 
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("pageviews", "uniquePageviews"), 
                   dimensions = c("pagePath"),
                   segments = segment,
                   anti_sample = TRUE) %>% 
  mutate(
    page_type = 
      case_when(
        str_detect(pagePath, "-hospital|-center") ~ "hospital",
        str_detect(pagePath, "-family-medicine") ~ "family medicine",
        str_detect(pagePath, "velocitycare") ~ "velocitycare",
        str_detect(pagePath, "/locations") ~ "other location",
        str_detect(pagePath, "/providers") ~ "providers",
        str_detect(pagePath, "/specialties") ~ "specialties",
        pagePath == "/" ~ "home",
        pageviews > 1000 ~ str_remove_all(pagePath, "/|-"),
        TRUE ~ 'other'
      ),
    is_location = if_else(str_detect(pagePath,"/locations"), TRUE, FALSE)
  )
landings <-
  google_analytics(ccid, 
                   date_range = date_range, 
                   metrics = c("sessions"), 
                   dimensions = c("landingPagePath"),
                   segments = segment,
                   anti_sample = TRUE) %>% 
  mutate(
    page_type = 
      case_when(
        str_detect(landingPagePath, "-hospital|-center") ~ "hospital",
        str_detect(landingPagePath, "-family-medicine") ~ "family medicine",
        str_detect(landingPagePath, "velocitycare") ~ "velocitycare",
        str_detect(landingPagePath, "/locations") ~ "other location",
        str_detect(landingPagePath, "/providers") ~ "providers",
        str_detect(landingPagePath, "/specialties") ~ "specialties",
        landingPagePath == "/" ~ "home",
        sessions > 500  ~ str_remove_all(landingPagePath, "/|-"),
        TRUE ~ 'other'
      ),
    is_location = if_else(str_detect(landingPagePath,"/locations"), TRUE, FALSE)
  )
landings_clean <- 
  landings %>% 
  filter(!str_detect(landingPagePath, "digital-health|/safe")) %>% 
  group_by(
    page_type,
  ) %>% 
  summarise(
    sessions = sum(sessions)
  ) %>% 
  mutate(
    is_location = if_else(str_detect(page_type, "family|hospital|location|velocity"), TRUE, FALSE)
  ) %>% 
  mutate(
    per = sessions / sum(sessions)
  )
  
df %>% 
  filter(!str_detect(pagePath, "digital-health|/safe")) %>% 
  group_by(
    page_type,
  ) %>% 
  summarise(
    pv = sum(pageviews)
  ) %>% 
  mutate(
    is_location = if_else(str_detect(page_type, "family|hospital|location|velocity"), TRUE, FALSE)
  ) %>% 
  mutate(
    per = pv / sum(pv)
  ) %>% 
  ggplot(
    aes(
      x = reorder(page_type, per),
      y = per,
      fill = is_location
    )
  )+
  geom_col()+
  theme_fivethirtyeight()+
  scale_fill_fivethirtyeight()+
  scale_y_continuous(labels= percent)+
  guides(fill = FALSE)+
  labs(x = "",y = "")+
  coord_flip()+
  ggtitle("Top content for users who visit digital health")
  


landings_clean %>% 
  ggplot(
    aes(
      x = reorder(page_type, per),
      y = per,
      fill = is_location
    )
  )+
  geom_col()+
  theme_fivethirtyeight()+
  scale_fill_fivethirtyeight()+
  scale_y_continuous(labels= percent)+
  guides(fill = FALSE)+
  labs(x = "",y = "")+
  coord_flip()+
  ggtitle("What else do digital-health sessions visit?")


